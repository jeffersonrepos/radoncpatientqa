﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientQAView
{
    class DbManager
    {
        SqlConnection connection;
        SqlCommand command;
        string strDBConnection;

        public DbManager()
        {
            
        } // constructor

        public bool GetUsersData( ref string strMrn, ref string strLastName, ref string strFirstName, ref string dob)
        {
            bool returnvalue = false;
            try
            {
                connection = new SqlConnection();
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DbMosaiq"].ConnectionString;
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                command.CommandText = "Select  Last_Name, First_Name, convert(varchar(11),Birth_DtTm, 101) from Patient p inner join ident i on p.pat_id1 = i.pat_id1 "
                                         +   "where i.IDA =  @strMrn";

                command.Parameters.Add("strMrn", SqlDbType.VarChar).Value = strMrn;
                

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        strLastName = reader.GetString(0);
                        strFirstName = reader.GetString(1);
                        dob = reader.GetString(2);
                        
                    }
                   
                }
                returnvalue = true;
            }
            catch
            { }
            finally
            {
                connection.Close();
                
            }
            return returnvalue;

        }
    }
}
