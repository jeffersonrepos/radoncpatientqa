﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientQAView
{
    class Patient
    {
        public int ID { get; set; }
        public int Pat_ID1 { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime Date { get; set; }


    }
}
