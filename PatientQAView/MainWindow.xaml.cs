﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace PatientQAView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            
                try
                {
                    
                    string lastName = null;
                    string firstName = null;
                    string dOB = null;
                    string mrn = null;

                DbManager db = new DbManager();

                mrn = txtMRN.Text;

                    bool status = db.GetUsersData(ref mrn, ref lastName, ref firstName, ref dOB);
                    if (status)
                    {
                        txtLastName.Text = lastName;
                        txtFirstName.Text = firstName;
                        txtDOB.Text = dOB;

                }
                }
                catch
                {

                }
            }
        }
    
}
